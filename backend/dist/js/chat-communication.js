(function($){
    
    'use strict'

    var user_img_src = $('.topuser-image').attr('src');

    var chat_content = $('.chat-content');

    var from_user_name = $('.sstopuser-test h4').text().trim();

    var chat_wrapper = $('.chat-container');

    var online_container = $('.online-container');

    var cms_wrapper = $(".cms-wrapper");

    var from_user_id = cms_wrapper.data('from_userid');

    var from_category = cms_wrapper.data('from_usercat');

    var small_popup_online_user = $('.small-popup-online-user');

    //var broadcast_btn = $(".broadcast");

    var broadcast_box = $(".broadcast-msg-box");

    //var save_broadcast_btn = $(".broadcast-msg-box .btn");

    var isOpen = true;

    var socket = null;

    var schoolName = $(".sidebar-session").text().trim();

    handleSocketInitialization();

    handleNotificationInitializations();

    minimizeAndMaximizeChatBox();

    minimizeAndMaximizeOnlineBox();

    initiateChatBox();

    //sendBroadcastMessage();

    $('.online-body').slimScroll({
        height: '577px'
    });

    saveActiveSideBar(false);

    $(document).ready(function(){
        $('.online-super-header span').click();

        // broadcast_btn.click(function(){
        //     broadcast_box.css('display', 'block');
        // })
        $('#search-online-user').keyup(function(evt) {
           let text = $(this).val().trim();
           if (text) {
                let body = {'terms': text}; 

                doAjaxPosts('admin/content/getUserBySearchTerms', body, function(results) {
                    let resultset = JSON.parse(results);
                    let parent_height = $('.online-body').height();
                    let found_html = structureFoundUsers(resultset.results);
                    
                    if ( ! found_html ) parent_height = 0;
                    else $('.online-body > .online-users-container').css('display', 'none');
                    
                    $('.search_result_wrapper').html(found_html).css('height', parent_height);
                    initiateChatBox()
                    //structureFoundUsers(resultset.results);
                }) ;
           }
           else {
               $('.search_result_wrapper').css('height', '0'); 
               $('.online-body > .online-users-container').css('display', 'block');
           } 
           
        })
    });
    
    function monitorUserInput() {
        let chatInput = $('.chat-box #chat-typings');

        chatInput.focus();
        
        chatInput.keyup(function(e) {
            if ( e.keyCode === 13 && !e.shiftKey ) handleCarriageReturnOnEmpty($(this));
        });
    }    

    function handleRealtimeCommunications(msg, rcp_id, rcp_name, cat) {

        let message = constructRealtimeMessage(msg, rcp_id, rcp_name, cat);
        
        doAjaxPosts('/chat/saveChatMessages', message, function(response){
            if ( response ) {
                socket.send(JSON.stringify(JSON.parse(response)));
                return ;
            }
            alert('Network error, could not send message.');
        });
    }


    function constructRealtimeMessage(message, recipient_id, recipient_name, category) {
        return {
            from_user_name: from_user_name,
            from_user_id: from_user_id,
            from_message: message,
            from_avatar: user_img_src,
            from_category: from_category,
            recipient_id: recipient_id,
            recipient_category: category,
            recipient_name: recipient_name,
            message_date: new moment().format(),
        }
    }

    function minimizeAndMaximizeChatBox() {
        $('.chat-header').click(function() {
            if ( isOpen ) {
                chat_wrapper.stop(true).animate({'height':  $(this).innerHeight() });
                isOpen = false;
                return false;
            }
            
            chat_wrapper.stop(true).removeAttr('style'); //animate({'height': ''});
            isOpen = true;
        })
    }


    function saveActiveSideBar(active) {
        localStorage.setItem('sidebar', JSON.stringify({ smallbarActive: active }))
    }

    function isSmallSidebarActive() {
        return JSON.parse(localStorage.getItem('sidebar')).smallbarActive;
    }

    function minimizeAndMaximizeOnlineBox() {

        $('.online-super-header span').click(function() {
            online_container.stop(true).animate({right: '-235px'});
            small_popup_online_user.stop(true).animate({bottom: '0px'});
            saveActiveSideBar(true);
            broadcast_box.css('display', 'none');
            animateChatBoxPosition('');
        });

        small_popup_online_user.click(function() {
            small_popup_online_user.stop(true).animate({bottom: '-80px'});
            online_container.stop(true).animate({right: '0px'});
            saveActiveSideBar(false);

            animateChatBoxPosition('');
        });
    }

    function handleCarriageReturnOnEmpty(that) {

        if ( that.text().trim() === '' ) {
            that.text('')
            return false
        }
        handleLocalUserMessage(that) 
    }

    function handleChatDateStarted(el, recipient_id) {

        let date_chat_started = moment().format('MMM DD, YYYY');

        let html_chat_date = `<div class="date-chat-started">Chat started ${date_chat_started}</div>`;

        el.find('.chat-content').html(html_chat_date);

        // do Ajax 
        loadChatHistory(recipient_id, function(html_chat_date) {
            el.find('.chat-content').html(html_chat_date);
        });
    }

    function loadChatHistory(recipient_id, callback) {
        let chat_body_reins = '';
        let html_chat_date = '';
        let body = {'from_user_id': from_user_id, 'recipient_id': recipient_id};
        doAjaxPosts('chat/getChatMessages', body, function(response) {
            response = JSON.parse(response);
            if (response.result.length) {
                response.result.map(function(val, id){
                   if ( parseInt(val.from_user_id) === from_user_id && val.from_category === from_category ) {
                        chat_body_reins += `<div class="chat-messages self">
                                                <div class="avatar" style="background: url(${val.from_avatar}) no-repeat center/cover"></div>
                                                <div class="chat-log">${ val.from_message }</div>
                                            </div>`;
                   } 
                   else {
                        chat_body_reins += `<div class="chat-messages friend">
                                                <div class="avatar" style="background: url(${val.from_avatar}) no-repeat center/cover"></div>
                                                <div class="chat-log">${val.from_message}</div>
                                            </div>`;
                   }
                });

                html_chat_date += chat_body_reins;

                callback(html_chat_date);
            }
        });
    }

    function handleLocalUserMessage(that) {

        let chat = 
            `<div class="chat-messages self">
                <div class="avatar" style="background: url(${user_img_src}) no-repeat center/cover"></div>
                <div class="chat-log">${ that.text().trim() }</div>
            </div>`; 

        let chat_container = that.closest('.chat-container');   

        chat_container.find('.chat-content').append(chat);

        let rcp_id = chat_container.find('.chat-tools-header').attr("data-id");

        let rcp_name = chat_container.find('.chat-tools-header').html();

        scrolldownOnMessageArrival();

        handleRealtimeCommunications(that.text().trim(), rcp_id, rcp_name, chat_container.attr('data-cat'))

        that.text("");
    }

    function handleRemoteUserMessage(msgObj) {

        let chat = 
            `<div class="chat-messages friend">
                <div class="avatar" style="background: url(${msgObj.from_avatar}) no-repeat center/cover"></div>
                <div class="chat-log">${msgObj.from_message}</div>
            </div>`;

        let user_name_id = msgObj.from_user_name.toLowerCase().replace(/ /g, '_') + '_' + msgObj.from_user_id;

        enableChatWindowForRightRecipient(msgObj.from_category, msgObj.from_user_name, msgObj.from_user_id, function() {

           let recipient_chat_window = $('#' + user_name_id);

            recipient_chat_window.find('.chat-content').append(chat);

            scrolldownOnMessageArrival();

            msgObj.title = schoolName;

            handleBrowserNotifications(msgObj); 
        });

        
    }

    function handleSocketInitialization() {

        if (!("WebSocket" in window)) {
            let sock_err = `<div class="network_err">Sorry, your browser do not support latest technology.</div>`;
            chat_content.html(sock_err);
            return false;
        }

        let protocol = (window.location.protocol === 'https:') ? 'wss://' : 'ws://'; 

        let ws_url = protocol + window.location.hostname + ':8200';
        socket = new WebSocket(ws_url);

        if (socket) {
            socket.onmessage = function (e) {
                let message = JSON.parse(e.data);
                handleRemoteUserMessage(message);
            }
        }
    }

    function handleNotificationInitializations() {

        if ( !("Notification" in window) ) {
            let sock_err = `<div class="network_err">Sorry, your browser do not support latest technology.</div>`;
            chat_content.html(sock_err);
            return false;
        }
        //console.log(Notification.permission);

        if ( Notification && Notification.permission !== "denied" ) {
            Notification.requestPermission(function(perm) {})
        }        
    }

    function handleBrowserNotifications(notifyObj) {
        if (  Notification.permission === 'granted' ) {
            let notify = new Notification(notifyObj.title, {
                body: notifyObj.message,
                icon: notifyObj.icon ? notifyObj.avatar : ''
            })
        }
    }

    function scrolldownOnMessageArrival() {
        let chat_body = $('.chat-body');
        chat_body.stop(true).animate({'scrollTop': chat_body.get(0).scrollHeight });
    }


    function initiateChatBox() { // Initiate chat window.

        $('.online-users-body a').click(function() {

            let that = $(this);
            
            let recipient_category = that.data('cat');
            let recipient_name = that.data('name');    
            let recipient_id = that.data('id');

            fetchChatbox(recipient_category, recipient_name, recipient_id);
        });
    }


    function getSpecificCategory(recipient_category, recipient_name, recipient_id) {

        let chat_wrapper = $('.chat-container:first');

        let recipient_name_extra = recipient_name.toLowerCase().replace(/ /g, '_')
        let recipient_name_for_id =  recipient_name_extra + '_' + recipient_id;
        
        if ( $('#' + recipient_name_for_id).length ) return ;

        chat_wrapper.clone(true).attr('data-name', recipient_name_extra).attr('id', recipient_name_for_id). attr('data-cat', recipient_category).appendTo("#chat-holder");

        if ( ! chat_wrapper.is(':visible') ) chat_wrapper.remove();

        resetCategoryChatBox($('#' + recipient_name_for_id), recipient_name, recipient_id);
        
        animateChatBoxPosition(recipient_name_for_id);
    }


    function enableChatWindowForRightRecipient(recipient_category, recipient_name, recipient_id, callback) {

        fetchChatbox(recipient_category, recipient_name, recipient_id, function(res) {
            callback(res);
        });
    }

    function resetCategoryChatBox(el, recipient_name, recipient_id) {

        el.find('.chat-tools-header').html(recipient_name).attr('data-id', recipient_id);

        el.find('.chat-content').html("");
        
        handleChatDateStarted(el, recipient_id);

        el.find('.chat-footer .chat-box #chat-typings').html("");
    }

    function animateChatBoxPosition() {

        let distance_right = 204;
        let adjusted_right = 320;

        if ( isSmallSidebarActive() ) distance_right = 74;

        let chat_wrapper = $('.chat-container');

        if ( chat_wrapper.length > 3 ) chat_wrapper.first().remove()
     
        $('.chat-container').each(function(id, el) {

            if ( id === 0 ) $(el).css('display', 'block').stop(true).animate({right: distance_right, bottom: 0}); 
            else {
                distance_right += adjusted_right;
                $(el).css('display', 'block').stop(true).animate({right: distance_right, bottom: 0});
            }

        });
    }


    function checkChatWindowBoxisOpened(incoming_id_attr) {
        let isChatOpened = false;

        $('.chat-container').each(function(id, el) {
            let id_attr = $(el).attr('id');
            if (id_attr !== incoming_id_attr)  isChatOpened = false ;
            else isChatOpened = true; 
        });

        return isChatOpened;
    }    

    function fetchChatbox(recipient_category, recipient_name, recipient_id, callback) {

        let incoming_id_attr =  recipient_name.toLowerCase().replace(/ /g, '_') + '_' + recipient_id;

        if ( checkChatWindowBoxisOpened(incoming_id_attr) ) {
            callback(true);
        }
        else {
            
            $('#chat-holder').load(base_url + '/backend/dist/js/chatbox.htm', function(res) {

                monitorUserInput();
                getSpecificCategory(recipient_category, recipient_name, recipient_id); 

                //loadChatHistory(el, function(html_chat_date) {
                    if (callback) callback(true);
                //});            
            });
        }
    }

    // function sendBroadcastMessage() {
    //     save_broadcast_btn.click(function(){
    //         let bcelem = $('.bmb-text')
    //         let bcText = bcelem.html().trim();
    //         let msgNotice = $('.errlog');
    //         msgNotice.removeClass('alert-danger').removeClass('alert-success');

    //         if (!bcText) {
    //             msgNotice.addClass('alert-danger').html('Please enter broadcast message to proceed.')
    //             .css('top', '0');
    //             setTimeout(function(){
    //                 msgNotice.css('top', '-58px');
    //             }, 2500)
    //             return false;
    //         }
    //         doAjaxPosts('admin/content/savebroadcastMessages', {message: bcText}, function(response){
    //             if ( response ) {
    //                 msgNotice.addClass('alert-success').html('Broadcast message sent publicly.').css('top', '0');
    //                 bcelem.html('');
    //                 setTimeout(function(){
    //                     msgNotice.css('top', '-58px');
    //                 }, 3000)
    //             }
    //         })
    //     })
    // }

    function doAjaxPosts(url, data, callback) {
        $.post(base_url + url, data, function(response){
            callback(response);
        });
    }

    function structureFoundUsers( resultset ) {
        var html = ``;
        var RsKeys = Object.keys(resultset);
        if (RsKeys.length) {
            html = `<div class="online-users-container">`;
            for (let key in resultset) {
                if ( RsKeys.indexOf(key) !== -1 && resultset[key].length > 0 ) {
                    let subHtml = `<div class="online-users-header"><h3>${key.toUpperCase()}</h3></div>`;
                    subHtml += `<ul class="online-users-body">`;
                    for (let i = 0, j = resultset[key].length; i < j; i++) {
                        let fullname = inferName(resultset[key][i]);
                        let id = resultset[key][i].id;
                        subHtml += `<li>
                            <a href="#" data-cat="${key}" data-name="${fullname}" data-id="${id}">
                                <span class="online-user-name">${fullname}</span>
                            </a>
                        </li>`;
                    }
                    subHtml += `</ul>`;
                    html += subHtml;
                }
            }
            html += '</div>';
        }
        return html;
    }

    function inferName( obj ) {
        var fullname = '';
        if ( 'lastname' in obj ) fullname = obj.firstname + ' ' + obj.lastname;
        else if ( 'father_name' in obj ) fullname = obj.father_name + ' ' + obj.mother_name;
        else fullname = obj.surname;
        return fullname;
    }
})(jQuery)