<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Paystack extends Parent_Controller {

    public $setting = "";
    private $ch = null;
    private $pay_method = null;

    function __construct() {
        parent::__construct();

        $this->setting = $this->setting_model->get();

        $this->ch = curl_init();

        $this->pay_method = $this->paymentsetting_model->getActiveMethod();
    }

    public function index() {
        $this->session->set_userdata('top_menu', 'Fees');
        $data = array();
        $data['params'] = $this->session->userdata('params');
        $data['setting'] = $this->setting;

        $this->load->view('parent/paystack', $data);
    }

    function initializePayment() {

        $this->form_validation->set_rules('student_fees_master_id', 'Feemaster', 'required|trim|xss_clean');
        $this->form_validation->set_rules('fee_groups_feetype_id', 'Fee Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('student_id', 'Student', 'required|trim|xss_clean');
        $this->form_validation->set_rules('guardian_email', 'Guardian Email', 'required|trim|xss_clean');
        $this->form_validation->set_rules('total', 'Amount', 'required|trim|xss_clean');
        $result = array();

        if ($this->form_validation->run() == false) {

            $data = array(
                'student_fees_master_id' => form_error('student_fees_master_id'),
                'fee_groups_feetype_id' => form_error('fee_groups_feetype_id'),
                'student_id' => form_error('student_id'),
                'amount' => form_error('amount'),
                'guardian_email' => form_error('guardian_email'),
            );
            
            $array = array('status' => false, 'error' => $data, 'errorType' => 'form');
            
            echo json_encode($array);

        } else {

            //var_export($this->pay_method->api_secret_key);

            $postdata =  array(
                'email' => $this->input->post('guardian_email'), 
                'amount' => str_pad($this->input->post('total'), (strlen($this->input->post('total'))+2) , '00'), 
                'reference' => '',
            );

            curl_setopt($this->ch, CURLOPT_URL, $this->config->item('PAYSTACK_PAYMENT_INITIALIZATION_URL'));
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

           $headers = ['Authorization: Bearer '. $this->pay_method->api_secret_key, 'Content-Type: application/json'];

            // $headers = array('Authorization' => 'Bearer '. $this->pay_method->api_secret_key, 'Content-Type' => 'application/json');

            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

            $request = curl_exec($this->ch);
            $error = curl_error($this->ch);
            
            curl_close ($this->ch);
            

            if ($error) {
                echo json_encode(array('status' => false, 'error' => $error, 'errorType' => 'curl'));
            }
            else {
                $result = json_decode($request);
                echo $result;
                if ( $result ) {
                    
                    $array = array();
                    
                    if ($result->data) $array = array('status' => $result->status, 'data' => $result->data);
                    
                    else $array = array('status' => $result->status, 'error' => $result->message, 'errorType' => 'curl');
                    
                    echo json_encode($array);
                }
                else {
                    echo json_encode(array('status' => false, 'error' => 'Empty server response.', 'errorType' => 'curl'));
                }
            }
        }

        
    }

    function checkout($trxref = '', $reference = '') {

        $trxref = $this->input->get('trxref');
        $reference = $this->input->get('reference');

        if ( trim($reference) == '' || trim($trxref) == '' ) {
            $this->session->set_flashdata('payment_msg', '<div class="alert alert-danger text-left">Mismatched receipt.</div>');    
            redirect(base_url("parent/payment/paymentfailed"));
        }
        else {

            $headers = [
                'accept: application/json',
                'authorization: Bearer '.$this->pay_method->api_secret_key,
                'cache-control: no-cache'
            ];

            curl_setopt_array($this->ch, array(
                CURLOPT_URL => $this->config->item('PAYSTACK_PAYMENT_VERIFY_URL') . rawurlencode($reference),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
              ));
              
              $response = curl_exec($this->ch);
              $err = curl_error($this->ch);
              
            if ($err) {
                $this->session->set_flashdata('payment_msg', '<div class="alert alert-danger text-left">Unable to connect to Paystack server.</div>');
                redirect(base_url("parent/payment/paymentfailed"));
            }
            else {

                $tranx = json_decode($response);
                
                if(! $tranx->status ){
                    $this->session->set_flashdata('payment_msg', '<div class="alert alert-danger text-left">'. $tranx->message .'</div>');
                    redirect(base_url("parent/payment/paymentfailed"));
                }
                
                if($tranx->data->status == 'success'){
                    $this->getsuccesspayment($tranx);
                }
            }
        }
    }

    //paypal successpayment
    public function getsuccesspayment($tranx) {

        $params = $this->session->userdata('params');
        $data = array();
        $student_fees_master_id = $params['student_fees_master_id'];
        $fee_groups_feetype_id = $params['fee_groups_feetype_id'];
        $student_id = $params['student_id'];
        $total = $params['total'];

        $data['student_fees_master_id'] = $student_fees_master_id;
        $data['fee_groups_feetype_id'] = $fee_groups_feetype_id;
        $data['student_id'] = $student_id;
        $data['total'] = $total;
        $data['symbol'] = $this->setting[0]['currency_symbol'];
        $data['currency_name'] = $this->setting[0]['currency'];
        $data['name'] = $params['name'];
        $data['guardian_phone'] = $params['guardian_phone'];
        
        // echo "<pre>".var_export($this->setting)."</pre>";

        // echo "<pre>".var_export($data)."</pre>";

        // echo "<pre>".var_export($tranx)."</pre>";
        
        if ($tranx->data->status == 'success') {
            
            $ref_id = $tranx->data->reference;

            $json_array = array(
                'amount' => $params['total'],
                'date' => date('Y-m-d'),
                'amount_discount' => 0,
                'amount_fine' => 0,
                'description' => "Online fees deposit through Paystack Ref ID: " . $ref_id,
                'payment_mode' => 'Paystack',
            );
            $data = array(
                'student_fees_master_id' => $params['student_fees_master_id'],
                'fee_groups_feetype_id' => $params['fee_groups_feetype_id'],
                'amount_detail' => $json_array
            );

            //var_export($params);
            $send_to = $params['guardian_phone'];
            $inserted_id = $this->studentfeemaster_model->fee_deposit($data, $send_to, 0);
            $invoice_detail = json_decode($inserted_id);
            redirect(base_url("parent/payment/successinvoice/" . $invoice_detail->invoice_id . "/" . $invoice_detail->sub_invoice_id));
        }
    }

}

?>