<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends Student_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['title'] = 'Upload Content';
        $data['title_list'] = 'Upload Content List';
        $list = $this->content_model->get();
        $data['list'] = $list;
        $ght = $this->customlib->getcontenttype();
        $data['ght'] = $ght;
        $class = $this->class_model->get();
        $data['classlist'] = $class;
        $this->load->view('layout/student/header');
        $this->load->view('user/content/createcontent', $data);
        $this->load->view('layout/student/footer');
    }

    public function download($file) {
        $this->load->helper('download');
        $filepath = "./uploads/school_content/material/" . $this->uri->segment(7);
        $data = file_get_contents($filepath);
        $name = $this->uri->segment(7);
        force_download($name, $data);
    }

    public function assignment() {
        $this->session->set_userdata('top_menu', 'Downloads');
        $this->session->set_userdata('sub_menu', 'content/assignment');
        $student_id = $this->customlib->getStudentSessionUserID();
        $student = $this->student_model->get($student_id);
        $class_id = $student['class_id'];
        $section_id = $student['section_id'];
        $data['title_list'] = 'List of Assignment';
        $list = $this->content_model->getListByCategoryforUser($class_id, $section_id, "Assignments");
        $data['list'] = $list;
        $this->load->view('layout/student/header');
        $this->load->view('user/content/assignment', $data);
        $this->load->view('layout/student/footer');
    }

    public function studymaterial() {
        $this->session->set_userdata('top_menu', 'Downloads');
        $this->session->set_userdata('sub_menu', 'content/studymaterial');
        $student_id = $this->customlib->getStudentSessionUserID();
        $student = $this->student_model->get($student_id);
        $class_id = $student['class_id'];
        $section_id = $student['section_id'];
        $data['title_list'] = 'List of Assignment';
        $list = $this->content_model->getListByCategoryforUser($class_id, $section_id, "Study Material");
        $data['list'] = $list;
        $this->load->view('layout/student/header');
        $this->load->view('user/content/studymaterial', $data);
        $this->load->view('layout/student/footer');
    }

    public function syllabus() {
        $this->session->set_userdata('top_menu', 'Downloads');
        $this->session->set_userdata('sub_menu', 'content/syllabus');
        $student_id = $this->customlib->getStudentSessionUserID();
        $student = $this->student_model->get($student_id);
        $class_id = $student['class_id'];
        $section_id = $student['section_id'];
        $data['title_list'] = 'List of Syllabus';
        $list = $this->content_model->getListByCategoryforUser($class_id, $section_id, "Syllabus");
        $data['list'] = $list;
        $this->load->view('layout/student/header');
        $this->load->view('user/content/syllabus', $data);
        $this->load->view('layout/student/footer');
    }

    public function other() {
        $this->session->set_userdata('top_menu', 'Downloads');
        $this->session->set_userdata('sub_menu', 'content/other');
        $student_id = $this->customlib->getStudentSessionUserID();
        $student = $this->student_model->get($student_id);
        $class_id = $student['class_id'];
        $section_id = $student['section_id'];
        $data['title_list'] = 'List of Other Download';
        $list = $this->content_model->getListByCategoryforUser($class_id, $section_id, "Other Download");
        $data['list'] = $list;
        $this->load->view('layout/student/header');
        $this->load->view('user/content/other', $data);
        $this->load->view('layout/student/footer');
    }

    public function uploadassignment($assignment_id=null) {
        $this->session->set_userdata('top_menu', 'Downloads');
        $this->session->set_userdata('sub_menu', 'content/assignment');
        $student_det = $this->session->userdata('student');
        $student_id = $student_det['student_id'];

        $this->form_validation->set_rules('file', 'Image', 'callback_handle_upload');
        $this->form_validation->set_rules('agreement', 'Agreement', 'trim|required|xss_clean');

        $data['id'] = $assignment_id;
        $record = $this->content_model->get_uploaded_assignment($student_id);
        //var_export($record); exit;
        $data['list'] = $record;

        $contents = $this->content_model->getContentById($assignment_id);
       // var_export($contents);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layout/student/header');
            $this->load->view('user/content/upload_completed_assignment', $data);
            $this->load->view('layout/student/footer');
        } else {

            $ag = $this->input->post('agreement');

            if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                $fileInfo = pathinfo($_FILES["file"]["name"]);
                $img_name = 'Completed-' . $contents[0]->type .'-id-' . $assignment_id . '-std-id-' . $student_id . '-std-name-' . $student_det['username'] . '.' . $fileInfo['extension'];
                $moved = move_uploaded_file($_FILES["file"]["tmp_name"], "./uploads/school_content/material/" . $img_name);

                if ( $moved ) {
                    
                    $content_data = array(
                        'content_id' => $assignment_id,
                        'title' => $contents[0]->title,
                        'type' => $contents[0]->type,
                        'class_id' => $contents[0]->class_id,
                        'cls_sec_id' => $contents[0]->cls_sec_id,
                        'date' => date('Y-m-d'),
                        'file' => 'uploads/school_content/material/' . $img_name,
                        'created_by' => $student_id,
                        'content_for' => $contents[0]->created_by,
                        'name' => $student_det['username']
                    );

                    $result = $this->content_model->upload_completed_assignment($content_data);

                    if ( $result ) {
                        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">Assignment Uploaded successfully</div>');
                    }
                    redirect('user/content/uploadassignment/');
                }   
            }
        }
    }

    public function deleteuploadedassignment($id)
    {
        if ($id) {
            $this->content_model->delete_uploaded_assignment($id);
        }
        redirect('user/content/uploadassignment/');   
    }


    function handle_upload() {
        if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
            $allowedExts = array('jpg', 'jpeg', 'png', "pdf", "doc", "docx", "rar", "zip");
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);
            if ($_FILES["file"]["error"] > 0) {
                $error .= "Error opening the file<br />";
            }
            if (($_FILES["file"]["type"] != "application/pdf") && ($_FILES["file"]["type"] != "image/gif") && ($_FILES["file"]["type"] != "image/jpeg") && ($_FILES["file"]["type"] != "image/jpg") && ($_FILES["file"]["type"] != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && ($_FILES["file"]["type"] != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && ($_FILES["file"]["type"] != "image/pjpeg") && ($_FILES["file"]["type"] != "image/x-png") && ($_FILES["file"]["type"] != "application/x-rar-compressed") && ($_FILES["file"]["type"] != "application/octet-stream") && ($_FILES["file"]["type"] != "application/zip") && ($_FILES["file"]["type"] != "application/octet-stream") && ($_FILES["file"]["type"] != "image/png")) {
                $this->form_validation->set_message('handle_upload', 'File type not allowed');
                return false;
            }
            if (!in_array($extension, $allowedExts)) {
                $this->form_validation->set_message('handle_upload', 'Extension not allowed');
                return false;
            }
            return true;
        } else {
            $this->form_validation->set_message('handle_upload', 'The File field is required.');
            return false;
        }
    }
}

?>