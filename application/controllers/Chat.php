<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chat extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function saveChatMessages()
    {
        $from_user_name = $this->input->post('from_user_name');
        $from_user_id = $this->input->post('from_user_id');
        $from_message = $this->input->post('from_message');
        $from_avatar = $this->input->post('from_avatar');
        $from_category = $this->input->post('from_category');
        $recipient_id = $this->input->post('recipient_id');
        $recipient_category = $this->input->post('recipient_category');
        $recipient_name = $this->input->post('recipient_name');
        $message_date = $this->input->post('message_date');

        $messages = array(
            'from_user_name' => $from_user_name,
            'from_user_id' => $from_user_id,
            'from_message' => $from_message,
            'from_avatar' => $from_avatar,
            'from_category' => $from_category,
            'recipient_id' => $recipient_id,
            'recipient_category' => $recipient_category,
            'recipient_name' => $recipient_name,
            'message_date' => date('Y-m-d h:i:s', strtotime($message_date)),
            'message_status' => 0
        );

        if ( $this->content_model->addMessages($messages) ) {
           echo json_encode($messages); 
        }
    }

    public function getChatMessages()
    {
        $from_user_id = $this->input->post('from_user_id');
        $recipient_id = $this->input->post('recipient_id');
    
        $result = $this->content_model->getMessages($from_user_id, $recipient_id);

        echo json_encode(array('result' => $result));
    }
}