<footer class="main-footer">
    &copy;  <?php echo date('Y'); ?> 
    <?php echo $this->customlib->getAppName(); ?> <?php echo $this->customlib->getAppVersion(); ?>
</footer>
<div class="control-sidebar-bg"></div>
</div>

<?php 
 $online_users = getOnlineUsers();
 $this_user_det = $this->session->get_userdata('student');
?>

<div class="cms-wrapper" data-from_userid="<?php echo $this_user_det['student']['id'] ?>" data-from_usercat="<?php echo $this_user_det['student']['role'] ?>">
    
    <div class="online-container">
        <h3 class="online-super-header">ONLINE USERS <span><i class="fa fa-arrow-right"></i></span></h3>
        <div class="online-body">
            <?php 
                if ( count($online_users['parent']) > 0 ) {
            ?>
            <div class="online-users-container">
                <div class="online-users-header"><h3>PARENTS</h3></div>
                <ul class="online-users-body">
                    <?php
                        foreach ( $online_users['parent'] as $id => $user_det ) {

                            $avatar = "uploads/student_images/no_image.png";
                            
                            if ($user_det['avatar']) $avatar = $user_det['avatar'];
                    ?>
                    <li>
                        <a href="#" data-cat="parent" data-name="<?php echo $user_det['name'] ?>" data-id="<?php echo $user_det['id'] ?>">
                            <span class="online-user-avatar" style="background: url(<?php echo base_url() . $avatar ?>) no-repeat center/cover"></span>
                            <span class="online-user-name"><?php echo $user_det['name']; ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>

            <?php 
                if ( count($online_users['student']) > 0 ) {
            ?>
            <div class="online-users-container">
                <div class="online-users-header"><h3>STUDENTS</h3></div>
                <ul class="online-users-body">
                    <?php
                        foreach ( $online_users['student'] as $id => $user_det ) {

                            $avatar = "uploads/student_images/no_image.png";
                            
                            if ($user_det['avatar']) $avatar = $user_det['avatar'];
                    ?>
                    <li>
                        <a href="#" data-cat="student" data-name="<?php echo $user_det['name'] ?>" data-id="<?php echo $user_det['id'] ?>">
                            <span class="online-user-avatar" style="background: url(<?php echo base_url() . $avatar ?>) no-repeat center/cover"></span>
                            <span class="online-user-name"><?php echo $user_det['name']; ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>

            <?php 
                if ( count($online_users['teacher']) > 0 ) {
            ?>
            <div class="online-users-container">
                <div class="online-users-header"><h3>TEACHER</h3></div>
                <ul class="online-users-body">
                    <?php
                        foreach ( $online_users['teacher'] as $id => $user_det ) {

                            $avatar = "uploads/student_images/no_image.png";
                            
                            if ($user_det['avatar']) $avatar = $user_det['avatar'];
                    ?>
                    <li>
                        <a href="#" data-cat="teacher" data-name="<?php echo $user_det['name'] ?>" data-id="<?php echo $user_det['id'] ?>">
                            <span class="online-user-avatar" style="background: url(<?php echo base_url() . $avatar ?>) no-repeat center/cover"></span>
                            <span class="online-user-name"><?php echo $user_det['name']; ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>

            <!-- <div class="online-users-container">
                <div class="online-users-header"><h3>TEACHER</h3></div>
                <ul class="online-users-body">
                    <li>
                        <a href="#" data-cat="teacher" data-name="Miss Gloria Ifeanyi" data-id="23">
                            <span class="online-user-avatar"></span>
                            <span class="online-user-name">Miss Gloria Ifeanyi</span>
                        </a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>
    <div class="small-popup-online-user"><i class="fa fa-chevron-up"></i></div>
    <div id="chat-holder"><!-- insert chatbox here --></div>
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";

</script>

<link href="<?php echo base_url(); ?>backend/toast-alert/toastr.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>backend/toast-alert/toastr.js"></script>

<script src="<?php echo base_url(); ?>backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>backend/dist/js/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/knob/jquery.knob.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/fastclick/fastclick.min.js"></script>

<script src="<?php echo base_url(); ?>backend/dist/js/app.min.js"></script>
<!--nprogress-->
<script src="<?php echo base_url(); ?>backend/dist/js/nprogress.js"></script>
<!--file dropify-->
<script src="<?php echo base_url(); ?>backend/dist/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>backend/dist/js/demo.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/chat-communication.js" ></script>

<!--print table-->
<!--print table-->
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/jquery.dataTables.min-stu.par.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.colVis.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/dataTables.responsive.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/ss.custom.js" ></script>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
<?php
if ($this->session->flashdata('success_msg')) {
    ?>
            successMsg("<?php echo $this->session->flashdata('success_msg'); ?>");
    <?php
} else if ($this->session->flashdata('error_msg')) {
    ?>
            errorMsg("<?php echo $this->session->flashdata('error_msg'); ?>");
    <?php
} else if ($this->session->flashdata('warning_msg')) {
    ?>
            infoMsg("<?php echo $this->session->flashdata('warning_msg'); ?>");
    <?php
} else if ($this->session->flashdata('info_msg')) {
    ?>
            warningMsg("<?php echo $this->session->flashdata('info_msg'); ?>");
    <?php
}
?>
    });
</script>



<script type="text/javascript">


    function complete_event(id, status) {

        $.ajax({
            url: "<?php echo site_url("user/calendar/markcomplete/") ?>" + id,
            type: "POST",
            data: {id: id, active: status},
            dataType: 'json',

            success: function (res)
            {

                if (res.status == "fail") {

                    var message = "";
                    $.each(res.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);

                } else {

                    successMsg(res.message);

                    window.location.reload(true);
                }

            }

        });
    }

    function markc(id) {

        $('#newcheck' + id).change(function () {

            if (this.checked) {

                complete_event(id, 'yes');
            } else {

                complete_event(id, 'no');
            }

        });
    }

</script>


<!-- Button trigger modal -->
<!-- Modal -->

<div class="modal fade" id="user_sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModalLabel">
    <form action="#" id="form_modal_usersession" class="form-horizontal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="sessionModalLabel"><?php echo $this->lang->line('session'); ?></h4>
                </div>
                <div class="modal-body user_sessionmodal_body pb0">

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary submit_usersession" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Please wait.."><?php echo $this->lang->line('save'); ?></button>
                </div>
            </div>
        </div>
    </form>
</div>

