<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
    }

    /**
     * This funtion takes id as a parameter and will fetch the record.
     * If id is not provided, then it will fetch all the records form the table.
     * @param int $id
     * @return mixed
     */
    public function get($id = null) {
        $this->db->select('contents.*,classes.class,sections.section,(select GROUP_CONCAT(role) FROM content_for WHERE content_id=contents.id) as role,class_sections.id as `aa`')->from('contents');
        $this->db->join('class_sections', 'contents.cls_sec_id = class_sections.id', 'left outer');
        $this->db->join('classes', 'class_sections.class_id = classes.id', 'left outer');
        $this->db->join('sections', 'class_sections.section_id = sections.id', 'left outer');
        if ($id != null) {
            $this->db->where('contents.id', $id);
        }
        $this->db->order_by('contents.id', "desc");
        $this->db->limit(10);
        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function getContentByRole($id = null, $role = null) {
        $query = "SELECT contents.*,(select GROUP_CONCAT(role) FROM content_for WHERE content_id=contents.id) as role,class_sections.id as `class_section_id`,classes.class,sections.section  FROM `content_for`  INNER JOIN contents on contents.id=content_for.content_id left JOIN class_sections on class_sections.id=contents.cls_sec_id left join classes on classes.id=class_sections.class_id LEFT JOIN sections on sections.id=class_sections.section_id WHERE (role='student' and created_by='" . $id . "' ) or (created_by=0 and role='" . $role . "')  GROUP by contents.id";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function getContentById($id = null) {
        return $this->db->select('*')->from('contents')->where('id', $id)->get()->result();
    } 

    public function getListByCategory($category) {
        $this->db->select('contents.*,classes.class')->from('contents');
        $this->db->join('classes', 'contents.class_id = classes.id', 'left outer');
        $this->db->where('contents.type', $category);
        $this->db->order_by('contents.id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getListByCategoryforUser($class_id, $section_id, $category = '') {

        if (empty($class_id)) {

            $class_id = "0";
        }

        if (empty($section_id)) {

            $section_id = "0";
        }
        $query = "SELECT contents.*,class_sections.id as `class_section_id`,classes.class,sections.section FROM `content_for` INNER JOIN contents on content_for.content_id=contents.id left JOIN class_sections on class_sections.id=contents.cls_sec_id left join classes on classes.id=class_sections.class_id LEFT JOIN sections on sections.id=class_sections.section_id WHERE  (role='student' and contents.type='" . $category . "' and contents.is_public='yes') or (classes.id =" . $class_id . " and sections.id=" . $section_id . " and role='student' and contents.type='" . $category . "')";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /**
     * This function will delete the record based on the id
     * @param $id
     */
    public function remove($id) {
        // delete from contents table.
        $this->db->where('id', $id);
        $this->db->delete('contents');
        // delete from content_for table.
        $this->db->where('content_id', $id);
        $this->db->delete('content_for');
    }

    public function search_by_content_type($text) {
        $this->db->select()->from('contents');
        $this->db->or_like('contents.content_type', $text);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * This function will take the post data passed from the controller
     * If id is present, then it will do an update
     * else an insert. One function doing both add and edit.
     * @param $data
     */
    public function add($data, $content_role = array()) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('contents', $data);
        } else {
            $this->db->insert('contents', $data);
            $insert_id = $this->db->insert_id();
            if (isset($content_role) && !empty($content_role)) {
                $total_rec = count($content_role);
                for ($i = 0; $i < $total_rec; $i++) {
                    $content_role[$i]['content_id'] = $insert_id;
                }
                $this->db->insert_batch('content_for', $content_role);
            }
            return $insert_id;
        }
    }

    public function get_recent_broadcast_messages() {
        return $this->db->select('*')
                ->from('contents')
                ->where('title', 'Broadcast')
                ->order_by('date', 'desc')
                ->limit(1,1)
                ->get();
    }

    public function upload_completed_assignment($content_data) {
        $content_id = $content_data['content_id'];
        $class_id   = $content_data['class_id'];
        $cls_sec_id = $content_data['cls_sec_id'];
        $created_by = $content_data['created_by'];
        $where_clause = array('content_id' => $content_id, 'class_id' => $class_id, 'cls_sec_id' => $cls_sec_id, 'created_by' => $created_by);
        $result =  $this->db->select('count(*) AS exist')
                    ->from('completed_assignment')
                    ->where($where_clause)
                    ->get()
                    ->result();

        if ( $result[0]->exist ) {
            $this->db->where($where_clause);
            $content_data['updated_at'] = date('Y-m-d h:i:s');
            $this->db->update('completed_assignment', $content_data);
            return true;
        }
        else {
            $this->db->insert('completed_assignment', $content_data);
            return true;
        };

        return false;
    }

    public function get_uploaded_assignment($student_id=null, $content_id=null) {
        $this->db->select('completed_assignment.*,classes.class')->from('completed_assignment');
        $this->db->join('classes', 'completed_assignment.class_id = classes.id', 'left outer');
        if ( $student_id )  $this->db->where('completed_assignment.created_by', $student_id);
        if ( $content_id )$this->db->where('completed_assignment.content_id', $content_id);
        $this->db->order_by('completed_assignment.id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_uploaded_assignment($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('completed_assignment');
        return true;
    }

    public function addMessages($messages)
    {
        $this->db->insert('chat_log', $messages);
        return $this->db->insert_id();
    }

    public function getMessages($from_user_id, $recipient_id)
    {
        $query = "SELECT * FROM chat_log WHERE (from_user_id = '".$from_user_id."' AND recipient_id = '".$recipient_id."') OR (from_user_id = '".$recipient_id."' 
                 AND recipient_id = '".$from_user_id."') ORDER BY message_date";

        $exec = $this->db->query($query);

        return $exec->result_array();
    }

}
