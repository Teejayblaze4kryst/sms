<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Communication_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
        $this->current_date = $this->setting_model->getDateYmd();
    }

    public function logOnlinePresence($data) {
        $this->db->insert('online_users', $data);
    }

    public function removeLoggedinUser($id) {
        
        $sql = "DELETE FROM online_users WHERE userId='". $id ."'";

        $query = $this->db->query($sql);
    }

    public function getLoggedOnlineUsers() {

        $result_arr = array('parent' => array(), 'student' => array(), 'teacher' => array());

        $sql = "SELECT * FROM online_users";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $results =  $query->result();

            foreach( $results as $key => $result ) {

                $sql = "SELECT * FROM students ";

                if ( $result->userCategory == 'parent' ) {

                    $sql .= " WHERE parent_id = '". $result->userId ."'";

                    $query = $this->db->query($sql);
                    
                    $result =  $query->result();

                    if ($result[0]->guardian_relation == "Father") {
                        $image = $result[0]->father_pic;
                    } else if ($result[0]->guardian_relation == "Mother") {
                        $image = $result[0]->mother_pic;
                    } else if ($result[0]->guardian_relation == "Other") {
                        $image = $result[0]->guardian_pic;
                    }

                    $result_arr['parent'][$result[0]->parent_id] = array( 'id' => $result[0]->parent_id, 'name' => $result[0]->guardian_name, 'avatar' => $image);
                } 
                else if ( $result->userCategory == 'student' ) {

                    $sql .= " WHERE id = '". $result->userId ."'";

                    $query = $this->db->query($sql);
                    
                    $result =  $query->result();
                    
                    $result_arr['student'][$result[0]->id] = array( 'id' => $result[0]->id, 'name' => $result[0]->firstname . " " . $result[0]->lastname, 'avatar' => $result[0]->image); ;
                } 
            }

            return $result_arr;

        } else {
            return false;
        }
    }

    public function getUserBysearchTerms($terms) {
        
        $studentRstArr = array();
        $teacherRstArr = array();
        $parentRstArr  = array();


        $stdQuery = "SELECT firstname, lastname, id, admission_no FROM students WHERE firstname LIKE '%" . $terms . "%' OR lastname LIKE '%" . $terms . "%'";
        $stdQexec = $this->db->query($stdQuery);
        $studentRstArr = $stdQexec->result();

        $parQuery = "SELECT father_name, mother_name, parent_id AS id FROM students WHERE father_name LIKE '%" . $terms . "%' OR mother_name LIKE '%" . $terms . "%'";
        $parQexec = $this->db->query($parQuery);
        $parentRstArr = $parQexec->result();

        $teaQuery = "SELECT surname, id FROM staff WHERE surname LIKE '%" . $terms . "%'";
        $teaQexec = $this->db->query($teaQuery);
        $teacherRstArr = $teaQexec->result();

        $merged_arr = array_merge(array('student' => $studentRstArr), array('teacher' => $teacherRstArr), array('parent' => $parentRstArr));
        return  json_encode(array('results' => $merged_arr));
    }

}
